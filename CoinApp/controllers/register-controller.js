var connection = require('./../config');
var bcrypt = require('bcryptjs');
module.exports.register=function(req,res){
    let today = new Date();
    let users={
        "email":req.body.email,
        "password":req.body.password,
        "created_at":today,
        "updated_at":today
    }


bcrypt.genSalt(10, function(error, salt) {
  bcrypt.hash(users.password, salt, function(error, hash) {
    users.password = hash;
    connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
      if (error) {
        console.log(error)
        res.json({
            status:false,
            message:'there are some error with query'

        })
      }
      else{
            res.redirect('./../auth')
      }
    })
})
})
}
