var express = require('express');
var router = express.Router()
var request = require('request')
var roundTo = require('round-to')
const localStorage = require('node-localstorage')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('frontPage', { title: 'Welcome' });
});
router.get('/auth', function(req, res){
  res.render('auth', {title:"Login"});
});


router.get('/register', function(req, res){
  res.render('register', {title:"Register"});
});
router.get('/info', function(req, res, next) {
    request('http://api.coingecko.com/api/v3/coins/' + req.query.id, (err, response, body) => {
        if (err) {
            console.log(err);
        }

        let coinsdata = {};
        let responseData = JSON.parse(body);
        coinsdata = {
            id : responseData.id,
            name : responseData.name,
            description : responseData.description.en,
            price : {
                usd : roundTo(responseData.market_data.current_price.usd, 2),
                eur : roundTo(responseData.market_data.current_price.eur, 2),
                rub : roundTo(responseData.market_data.current_price.rub, 2)
            },
            price_change_24_in_currency: roundTo(responseData.market_data.price_change_24h_in_currency.usd,2)
        };
        console.log(req.query.id);
        console.log(coinsdata);
        res.render('infoCoin', {coinsdata: coinsdata});
    });
});
router.get('/coinwork', function(req, res, next){
  res.render('coinwork', {title:"coinwork"});
});

router.post('/logout', function(req, res){
  localStorage.clear()
  res.redirect('/frontPage')
})
module.exports = router;
