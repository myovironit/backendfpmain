var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');



function get_token(user, secret)
{
  let token = jwt.sign(user, secret, {expiresIn: 3600})

  return token
};

function get_user(token, secret)
{
  let user = jwt.verify(token, secret)
  return user
};

module.exports =
{
    router: router,
    get_user : get_user,
    get_token: get_token
};
