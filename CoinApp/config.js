var mysql= require('mysql');

var connection = mysql.createConnection({
  host :'localhost',
  port: '3306',
  user  : 'root',
  password : 'root',
  database : 'test',
  insecureAuth : true,
});
connection.connect(function(err){
if(!err) {
    console.log("Database is connected");
} else {
    console.log("Error while connecting with database");
    console.log(err);
};
});

module.exports = connection;
