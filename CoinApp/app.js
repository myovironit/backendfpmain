var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser=require('body-parser');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authenticateController=require('./controllers/authenticate-controller');
var registerController=require('./controllers/register-controller');


var app = express();
app.use(bodyParser.json());

app.set('secret','12344321');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api/register', bodyParser.urlencoded({
  extended: true
}));
app.post('/api/register', registerController.register, function(req,res){
  res.redirect('./views/auth')
});
app.use('/api/authenticate', bodyParser.urlencoded({
  extended: true
}));
app.post('/api/authenticate', authenticateController.authenticate);
app.use('/coinwork', bodyParser.urlencoded({
  extended: true
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};


  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
